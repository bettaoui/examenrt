package org.id.exam.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Data {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private String BusinessEntityID;
    private String NationalIDNumber;
    private String LoginID;
    private String JobTitle;
    private String MaritalStatus;
    private String Gender;

}
