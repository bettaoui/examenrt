package org.id.kafkatransformer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaTransformerApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaTransformerApplication.class, args);
    }

}
